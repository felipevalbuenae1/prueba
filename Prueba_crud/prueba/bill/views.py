# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

#Imporar modelos
from .models import BillModel
#Importar modulos REST
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
#importar serializer
from .serializer import BillSerializer

# Create your views here.
class Bill_list(APIView):
    queryset = BillModel.objects.all()
    serializer_class = BillSerializer
    
    def get(self,request):
        queryset = BillModel.objects.all()
        serializer_class = BillSerializer(queryset, many=True)
        return Response(serializer_class.data)

class Bill_list(viewsets.ModelViewSet):
    queryset = BillModel.objects.all()
    serializer_class = BillSerializer