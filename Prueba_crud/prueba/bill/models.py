# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models 
from client.models import ClientModel

# Create your models here.


class BillModel(models.Model):
    nombre = models.CharField(max_length=10)
    apellido = models.CharField(max_length=10)
    correo = models.EmailField()
    telefono = models.IntegerField(max_length=10)
    clients = models.ManyToManyField(ClientModel)
    

