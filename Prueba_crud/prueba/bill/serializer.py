from rest_framework import serializers
from .models import BillModel

class BillSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BillModel
        fields = "__all__"
    class Bill(serializers.Serializer):
        name = serializers.CharField()
